﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFCodeFirst.Entities
{
    [Table("MatHang")]
    public class MatHang
    {
        [Key]
        public Guid MaMH {get; set;}
        public string TenMH { get; set; }
        public string MoTa { get; set; }
        public string Hinh { get; set; }
        public double DonGia { get; set; }
        public int SL { get; set; }

        public int MaLoai { get; set; }
        [ForeignKey("MaLoai")]
        public LoaiHang LoaiHang { get; set; }

    }
}
