﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFCodeFirst.Entities
{
    [Table ("LoaiHang")]
    public class LoaiHang
    {
        [Key]
        public int MaLoai { get; set; }
        public string TenLoai { get; set; }
        public string MoTa { get; set; }


        public ICollection<MatHang> MatHangs { get; set; }
        public LoaiHang ()
        {
            MatHangs = new HashSet<MatHang>();
        }
 
    }
}
