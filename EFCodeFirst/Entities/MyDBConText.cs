﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCodeFirst.Entities
{
    public class MyDBConText:DbContext
    {
        public DbSet<MatHang> MatHangs { get; set; }
        public DbSet<LoaiHang> LoaiHangs { get; set; }

        public MyDBConText(DbContextOptions options):base(options)
        {

        }

    }
}
