﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class DbInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoaiHang",
                columns: table => new
                {
                    MaLoai = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoai = table.Column<string>(nullable: true),
                    MoTa = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoaiHang", x => x.MaLoai);
                });

            migrationBuilder.CreateTable(
                name: "MatHang",
                columns: table => new
                {
                    MaMH = table.Column<Guid>(nullable: false),
                    TenMH = table.Column<string>(nullable: true),
                    MoTa = table.Column<string>(nullable: true),
                    Hinh = table.Column<string>(nullable: true),
                    DonGia = table.Column<double>(nullable: false),
                    SL = table.Column<int>(nullable: false),
                    MaLoai = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatHang", x => x.MaMH);
                    table.ForeignKey(
                        name: "FK_MatHang_LoaiHang_MaLoai",
                        column: x => x.MaLoai,
                        principalTable: "LoaiHang",
                        principalColumn: "MaLoai",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MatHang_MaLoai",
                table: "MatHang",
                column: "MaLoai");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatHang");

            migrationBuilder.DropTable(
                name: "LoaiHang");
        }
    }
}
